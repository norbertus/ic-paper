#! /usr/bin/python3.4

from os import listdir
from os.path import isfile, join
from operator import itemgetter
from matplotlib.ticker import FuncFormatter
from matplotlib.patches import Rectangle
from pylab import legend
from bs4 import BeautifulSoup

import os
import re
import csv
import collections
import numpy as np
import matplotlib.pyplot as plt
import requests
import sys

gender = []
role = []
university = []
region = []
area = []
prof = []
profuniv = []
profgend = []

ssd = str(sys.argv[1])
minyear = int(sys.argv[2])
maxyear = int(sys.argv[3])

roles = {
	'Ricercatore non confermato': 'RU',
	'Ordinario': 'PO',
	'Straordinario': 'PO',
	'Associato': 'PA',
	'Ricercatore a t.d. (art. 24 comma 3-b L. 240/10)': 'RTDa',
	'Ricercatore a t.d. (art. 24 comma 3-a L. 240/10)': 'RTDb',
	'Straordinario tempo determinato': 'PO',
	'Ricercatore': 'RU',
	'Associato non confermato': 'PA',
	'Ricercatore a t.d. (art.1 comma 14 L. 230/05)': 'RTD',
	'Associato confermato': 'PA'
}

univ2region = {
	'BARI': 'Puglia', 
	'BASILICATA': 'Basilicata',
	'BERGAMO': 'Lombardia',
	'Bocconi MILANO': 'Lombardia',
	'BOLOGNA': 'Emilia-Romagna',
	'BRESCIA': 'Lombardia',
	'"Ca\' Foscari" VENEZIA': 'Veneto',
	'CAGLIARI': 'Sardegna',
	'CAMERINO': 'Marche',
	'CAMPANIA': 'Campania',
	'CASSINO e LAZIO MERIDIONALE': 'Lazio',
	'CATANIA': 'Sicilia',
	'CATANZARO': 'Calabria',
	'Cattolica del Sacro Cuore': 'Lombardia',
	'CHIETI-PESCARA': 'Abruzzo',
	'della CALABRIA': 'Calabria',
	'EUROPEA di ROMA': 'Lazio',
	'FERRARA': 'Emilia-Romagna',
	'FIRENZE': 'Toscana',
	'FOGGIA': 'Puglia',
	'GENOVA': 'Liguria',
	'INSUBRIA': 'Lombardia',
	'IULM - MILANO': 'Lombardia',
	'I.U.S.S. - PAVIA': 'Lombardia',
	'L\'AQUILA': 'Abruzzo',
	'Libera Università di BOLZANO': 'Trentino Alto Adige',
	'Libera Univ. "Maria SS.Assunta"-LUMSA - ROMA': 'Lazio',
	'LINK CAMPUS': 'Lazio',
	'LIUC - CASTELLANZA': 'Lombardia',
	'"L\'Orientale" di NAPOLI': 'Campania',
	'LUISS "Guido Carli" - ROMA': 'Lazio',
	'MACERATA': 'Marche',
	'Mediterranea di REGGIO CALABRIA': 'Calabria',
	'MESSINA': 'Sicilia',
	'MILANO': 'Lombardia',
	'MILANO-BICOCCA': 'Lombardia',
	'MODENA e REGGIO EMILIA': 'Emilia-Romagna',
	'MOLISE': 'Molise',
	'NAPOLI "Federico II"': 'Campania',
	'Napoli Federico II': 'Campania',
	'PADOVA': 'Veneto',
	'PALERMO': 'Sicilia',
	'PARMA': 'Emilia-Romagna',
	'"Parthenope" di NAPOLI': 'Campania',
	'PAVIA': 'Lombardia',
	'PERUGIA': 'Umbria',
	'PIEMONTE ORIENTALE': 'Piemonte',
	'PISA': 'Toscana',
	'Politecnica delle MARCHE': 'Marche',
	'Politecnico di BARI': 'Puglia',
	'Politecnico di MILANO': 'Lombardia',
	'Politecnico di TORINO': 'Piemonte',
	'ROMA "La Sapienza"': 'Lazio',
	'ROMA "Tor Vergata"': 'Lazio',
	'ROMA TRE': 'Lazio',
	'SALENTO': 'Puglia',
	'SALERNO': 'Campania',
	'SANNIO di BENEVENTO': 'Campania',
	'SASSARI': 'Sardegna',
	'SCIENZE GASTRONOMICHE': 'Piemonte',
	'Scuola IMT - LUCCA': 'Toscana',
	'Scuola Normale Superiore di PISA': 'Toscana',
	'Scuola Superiore Sant\'Anna': 'Toscana',
	'SIENA': 'Toscana',
	'SISSA - TRIESTE': 'Friuli Venezia Giulia',
	'S. Raffaele MILANO': 'Lombardia',
	'Stranieri di PERUGIA': 'Umbria',
	'Stranieri di SIENA': 'Toscana',
	'Stranieri REGGIO CALABRIA': 'Calabria',
	'SUM - Ist. Italiano di SCIENZE UMANE FIRENZE': 'Toscana',
	'Suor Orsola Benincasa - NAPOLI': 'Campania',
	'TERAMO': 'Abruzzo',
	'TORINO': 'Piemonte',
	'TRENTO': 'Trentino Alto Adige',
	'TRIESTE': 'Friuli Venezia Giulia',
	'TUSCIA': 'Lazio',
	'UDINE': 'Friuli Venezia Giulia',
	'UKE': 'Sicilia',
	'UNICUSANO - Telematica Roma': 'Lazio',
	'Università IUAV di VENEZIA': 'Veneto',
	'Univ. "Campus Bio-Medico" di ROMA': 'Lazio',
	'Univ. Studi GUGLIELMO MARCONI - Telematica': 'Lazio',
	'Univ. Studi Internazionali di ROMA (UNINT)': 'Lazio',
	'Univ. Telematica "E-CAMPUS"': 'Lombardia',
	'Univ. Telematica "GIUSTINO FORTUNATO"': 'Campania',
	'Univ. Telematica GUGLIELMO MARCONI': 'Lazio',
	'Univ. Telematica PEGASO': 'Campania',
	'Univ. Telematica San Raffaele Roma': 'Lazio',
	'Univ. Telematica UNITELMA SAPIENZA': 'Lazio',
	'Univ. Telematica Internazionale UNINETTUNO': 'Lazio',
	'Urbino Carlo Bo': 'Marche',
	'VALLE D\'AOSTA': 'Valle d\'Aosta',
	'VERONA': 'Veneto'
}

region2area = {
	'Valle d\'Aosta': 'Nord',
	'Piemonte': 'Nord',
	'Liguria': 'Nord',
	'Lombardia': 'Nord',
	'Trentino Alto Adige': 'Nord',
	'Veneto': 'Nord',
	'Friuli Venezia Giulia': 'Nord',
	'Emilia-Romagna': 'Nord',
	'Toscana': 'Centro',
	'Marche': 'Centro',
	'Umbria': 'Centro',
	'Lazio': 'Centro',
	'Abruzzo': 'Sud',
	'Molise': 'Sud',
	'Campania': 'Sud',
	'Basilicata': 'Sud',
	'Puglia': 'Sud',
	'Calabria': 'Sud',
	'Sicilia': 'Sud',
	'Sardegna': 'Sud',
}

role_hier = {
	'RTD': 1,
	'RTDa': 2,
	'RTDb': 3,
	'RU': 4,
	'PA': 5,
	'PO': 6
}

def role_value(x):
	return role_hier.get(x)

def map_roles(x):
	return roles.get(x)

def map_region(x):
	if re.search('CAMPANIA', x):
		x = 'CAMPANIA'
	elif re.search('UKE', x):
		x = 'UKE'
	if (not univ2region.get(x)):
		print("Errore: università non mappata "+x)
	return univ2region.get(x)

def map_area(x):
	return region2area.get(x)

def add_legend(color, label, position):
	p = []
	for c in color:
		rect = Rectangle((0, 0), 1, 1, fc=c)
		p.append(rect)
	if (position != ''):
		legend(p, label, loc=position)
	else:
		legend(p, label)

def plot_save(plt, name):
	plt.savefig('figures/'+name+'.png', bbox_inches='tight')
	plt.savefig('figures/'+name+'.pdf', bbox_inches='tight')
	plt.clf()

def plot_region_distribution(x, year):

	if (year == minyear):
		name = 'figure-06'
	else:
		name = 'figure-07'

	# Sort the tuple list
	x = sorted(x, key=itemgetter(1), reverse=True)

	# Insert the first five elements into the data series
	data = []
	labels = []
	for k, v in x[:5]:
		data.append(v)
		labels.append(k)

	# Add the rest and append
	labels.append('Altro')
	data.append(0)
	for k, v in x[5:]:
		data[5] += v

	# Use some good colours
	cmap = plt.cm.prism
	colors = cmap(np.linspace(0., 1., len(data)*3/2))
	
	# Plot the stuff
	plt.figure(1, figsize=(6,6))
	plt.ax = plt.axes([0.1, 0.1, 0.8, 0.8])
	plt.title('Docenti universitari settore ' + ssd + ' per regione - ' + str(year))
	plt.pie(data, labels=labels, colors=colors, autopct='%1.1f%%', startangle=90)
	plot_save(plt, name)


def plot_areas_distribution(x):

	center = []
	north = []
	south = []
	centerper = []
	northper = []
	southper = []
	years = []
	for item in x:
		# Sort the tuple list
		i = sorted(item[1:], key=itemgetter(0))
		years.append(item[0])
		center.append(i[0][1])
		north.append(i[1][1])
		south.append(i[2][1])
		centerper.append(i[0][1]/(i[0][1]+i[1][1]+i[2][1]))
		northper.append(i[1][1]/(i[0][1]+i[1][1]+i[2][1]))
		southper.append(i[2][1]/(i[0][1]+i[1][1]+i[2][1]))

	# Plot number of prof by area
	plt.stackplot(years, north, center, south, colors=['b', 'g', 'r'])
	ax = plt.gca()
	ax.get_xaxis().get_major_formatter().set_useOffset(False)
	plt.xlabel('Anno')
	plt.ylabel('Docenti universitari settore ' + ssd)
	add_legend(['b', 'g', 'r'], ['Nord', 'Centro', 'Sud'], '')
	plot_save(plt, 'figure-08-bis')

	# Plot prof distribution by area
	plt.stackplot(years, northper, centerper, southper, colors=['b', 'g', 'r'])
	ax = plt.gca()
	start, end = ax.get_ylim()
	ax.yaxis.set_ticks(np.arange(start, end, 0.1))
	ax.get_xaxis().get_major_formatter().set_useOffset(False)
	ax.yaxis.set_major_formatter(FuncFormatter(lambda y, _: '{:.0%}'.format(y)))

	plt.xlabel('Anno')
	plt.ylabel('Docenti universitari settore ' + ssd)
	add_legend(['b', 'g', 'r'], ['Nord', 'Centro', 'Sud'], '')
	plot_save(plt, 'figure-08')


def plot_gender_distribution(x):

	years = []
	male = []
	maleper = []
	femaleper = []
	female = []
	total = []
	for item in gender:
		years.append(item[0])
		male.append(item[1])
		female.append(item[2])
		maleper.append(item[1]/(item[1]+item[2]))
		femaleper.append(item[2]/(item[1]+item[2]))
		total.append(item[1]+item[2])

	# Plot male/female numbers
	ax = plt.gca()
	ax.get_xaxis().get_major_formatter().set_useOffset(False)
	plt.stackplot(years, male, female, colors=['b','r'])

	plt.xlabel('Anno')
	plt.ylabel('Docenti universitari settore ' + ssd)
	add_legend(['b', 'r'], ['Uomini', 'Donne'], '')
	plot_save(plt, 'figure-04')

	# Plot male/female distribution
	ax = plt.gca()
	start, end = ax.get_ylim()
	ax.yaxis.set_ticks(np.arange(start, end + 0.1, 0.1))
	ax.get_xaxis().get_major_formatter().set_useOffset(False)
	ax.yaxis.set_major_formatter(FuncFormatter(lambda y, _: '{:.0%}'.format(y)))
	plt.stackplot(years, maleper, femaleper, colors=['b','r'])

	plt.xlabel('Anno')
	plt.ylabel('Docenti universitari settore ' + ssd)
	add_legend(['b', 'r'], ['Uomini', 'Donne'], '')
	plot_save(plt, 'figure-05')

	# Plot total
	ax = plt.gca()
	ax.get_xaxis().get_major_formatter().set_useOffset(False)
	plt.plot(years, total, 'b')
	plt.ylim(ymin=0)

	plt.xlabel('Anno')
	plt.ylabel('Docenti universitari settore ' + ssd)
	plot_save(plt, 'figure-01')


def plot_role_distribution(x):

	rtd = []
	rtda = []
	rtdb = []
	ru = []
	pa = []
	po = []
	rtdper = []
	rtdaper = []
	rtdbper = []
	ruper = []
	paper = []
	poper = []
	years = []
	for y in role:
		rtd.append(0)
		rtda.append(0)
		rtdb.append(0)
		ru.append(0)
		pa.append(0)
		po.append(0)
		for item in y:
			if isinstance (item, int):
				years.append(item)
				continue
			if (item[0] == 'RTD'):
				rtd[-1] = item[1]
			elif (item[0] == 'RTDa'):
				rtda[-1] = item[1]
			elif (item[0] == 'RTDb'):
				rtdb[-1] = item[1]
			elif (item[0] == 'RU'):
				ru[-1] = item[1]
			elif (item[0] == 'PA'):
				pa[-1] = item[1]
			elif (item[0] == 'PO'):
				po[-1] = item[1]
		total = rtd[-1] + rtda[-1] + rtdb[-1] + ru[-1] + pa[-1] + po[-1]
		if rtd[-1] == 0:
			rtdper.append(0)
		else:
			rtdper.append(rtd[-1]/total)
		if rtda[-1] == 0:
			rtdaper.append(0)
		else:
			rtdaper.append(rtda[-1]/total)
		if rtdb[-1] == 0:
			rtdbper.append(0)
		else:
			rtdbper.append(rtdb[-1]/total)
		if ru[-1] == 0:
			ruper.append(0)
		else:
			ruper.append(ru[-1]/total)
		if pa[-1] == 0:
			paper.append(0)
		else:
			paper.append(pa[-1]/total)
		if po[-1] == 0:
			poper.append(0)
		else:
			poper.append(po[-1]/total)
	# Plot absolute role numbers
	ax = plt.gca()
	ax.get_xaxis().get_major_formatter().set_useOffset(False)
	plt.stackplot(years, rtd, rtda, rtdb, ru, pa, po, colors=['midnightblue', 'b', 'cornflowerblue', 'r', 'y', 'g'])

	plt.xlabel('Anno')
	plt.ylabel('Docenti universitari settore ' + ssd)
	add_legend(['midnightblue', 'b', 'cornflowerblue', 'r', 'y', 'g'], ['RTD', 'RTDa', 'RTDb', 'RU', 'PA', 'PO'], '')
	plot_save(plt, 'figure-02')

	# Plot roles distribution
	ax = plt.gca()
	start, end = ax.get_ylim()
	ax.yaxis.set_ticks(np.arange(start, end + 0.1, 0.1))
	ax.get_xaxis().get_major_formatter().set_useOffset(False)
	ax.yaxis.set_major_formatter(FuncFormatter(lambda y, _: '{:.0%}'.format(y)))
	plt.stackplot(years, rtdper, rtdaper, rtdbper, ruper, paper, poper, colors=['midnightblue', 'b', 'cornflowerblue', 'r', 'y', 'g'])

	plt.xlabel('Anno')
	plt.ylabel('Docenti universitari settore ' + ssd)
	add_legend(['midnightblue', 'b', 'cornflowerblue', 'r', 'y', 'g'], ['RTD', 'RTDa', 'RTDb', 'RU', 'PA', 'PO'], 'upper left')
	plot_save(plt, 'figure-03')


# Count promotions and recruitments
def recruitments(x, pg, pu):
	same = []
	promotion = []
	recruitment = []
	totprom = []
	totrecr = []
	years = []
	prom_by_gender = []
	prom_by_univ = []
	prom_by_region = []
	prom_by_area = []
	recr_by_gender = []
	recr_by_univ = []
	recr_by_region = []
	recr_by_area = []
	total = []
	for i in range(1, len(x)):
		years.append(x[i][0])
		old = dict(x[i-1][1:])
		new = dict(x[i][1:])
		gend = dict(pg[i][1:])
		univ = dict(pu[i][1:])
		tmprecr = collections.defaultdict(int)
		tmprgend = collections.defaultdict(int)
		tmpruniv = collections.defaultdict(int)
		tmpsame = collections.defaultdict(int)
		tmpprom = collections.defaultdict(int)
		tmppgend = collections.defaultdict(int)
		tmppuniv = collections.defaultdict(int)
		tmprregion = collections.defaultdict(int)
		tmprarea = collections.defaultdict(int)
		tmppregion = collections.defaultdict(int)
		tmpparea = collections.defaultdict(int)
		tmptotal = 0
		for k, v in new.items():
			v1 = old.get(k, 'new')
			g = gend.get(k)
			u = univ.get(k)
			if (v1 == 'new'):
				tmprecr[v] += 1
				tmprgend[g] += 1
				tmpruniv[u] += 1
				tmprregion[map_region(u)] += 1
				tmprarea[map_area(map_region(u))] += 1
				tmptotal += 1
			elif (v == v1):
				tmpsame[v] += 1
			else:
				try:
					if (role_value(v) < role_value(v1)):
						print("Demotion: "+k)
						continue
				except TypeError:
					continue
				# TODO: from where we promote?
				tmpprom[v] += 1
				tmppgend[g] += 1
				tmppuniv[u] += 1
				tmppregion[map_region(u)] += 1
				tmpparea[map_area(map_region(u))] += 1
				tmptotal += 1
		same.append(tmpsame)
		promotion.append(tmpprom)
		recruitment.append(tmprecr)
		total.append(tmptotal)
		prom_by_gender.append(tmppgend)
		prom_by_univ.append(tmppuniv)
		prom_by_region.append(tmppregion)
		prom_by_area.append(tmpparea)
		recr_by_gender.append(tmprgend)
		recr_by_univ.append(tmpruniv)
		recr_by_region.append(tmprregion)
		recr_by_area.append(tmprarea)

	for item in promotion:
		totprom.append(sum(item.values()))
	for item in recruitment:
		totrecr.append(sum(item.values()))

	#print(total)
	#print('---')
	#print(totprom)
	#print('---')
	#print(totrecr)
	#print('---')
	#print(prom_by_gender)
	#print('---')
	#print(prom_by_univ)
	#print('---')
	#print(prom_by_region)
	#print('---')
	#print(prom_by_area)
	#print('---')
	#print(recr_by_gender)
	#print('---')
	#print(recr_by_univ)
	#print('---')
	#print(recr_by_region)
	#print('---')
	#print(recr_by_area)

	# Plot recruitment numbers
	ax = plt.gca()
	plt.xlim(xmin=minyear+1, xmax=maxyear)
	ax.get_xaxis().get_major_formatter().set_useOffset(False)
	plt.stackplot(years, totprom, totrecr, colors=['b','r'])
	plt.ylim(ymin=0)

	plt.xlabel('Anno')
	add_legend(['b', 'r'], ['Progressioni', 'Reclutamenti'], 'upper left')
	plot_save(plt, 'figure-09')

	for i in range(0, len(total)):
		if total[i] == 0:
			totprom[i] = 0
			totrecr[i] = 0
		else:
			totprom[i] /= total[i]
			#print(totrecr[i], total[i])
			totrecr[i] /= total[i]

	# Plot recruitment numbers
	ax = plt.gca()
	plt.xlim(xmin=minyear+1, xmax=maxyear)
	plt.ylim(ymin=0)
	start, end = ax.get_ylim()
	ax.yaxis.set_ticks(np.arange(start, end + 0.1, 0.1))
	ax.get_xaxis().get_major_formatter().set_useOffset(False)
	ax.yaxis.set_major_formatter(FuncFormatter(lambda y, _: '{:.0%}'.format(y)))
	plt.stackplot(years, totprom, totrecr, colors=['b','r'])

	plt.xlabel('Anno')
	add_legend(['b', 'r'], ['Progressioni', 'Reclutamenti'], '')
	plot_save(plt, 'figure-10')


#def mobility (x):
#	change = []
#	years = []
#	for i in range(1, len(x)):
#		print(x[i][0])
#		years.append(x[i][0])
#		old = dict(x[i-1][1:])
#		new = dict(x[i][1:])
#		tmpchange = collections.defaultdict(int)
#		for k, v in new.items():
#			v1 = old.get(k, 'new')
#			if (v1 != 'new' and v != v1):
#				print(k + ':' + v1 + ' --> ' + v)

# Get files from CINECA
path = ssd.lower().replace("/", "")
if not os.path.exists(path):
	os.makedirs(path)
	for i in range(8, 19):
		print(i)
		if i <= 10:
			f = "0"+str(i-1)
		else:
			f = str(i-1)
		payload = {
			'universita': '00',
			'facolta': '00',
			'settore': ssd.upper(),
			'area': '0000',
			'qualifica': '**',
			'situazione_al': i,
			'radiogroup': 'P',
			'genere': 'A',
			'servizio': 'N',
			'cognome': '',
			'nome': '',
			'conferma': '2',
			'facolta_st': '00',
			'settorec': '0000',
			'macro': '0000',
			'testuale': '1',
			'nomefile': '$nomefile',
			'pagina': '\A',
		}
		r = requests.post("http://cercauniversita.cineca.it/php5/docenti/vis_docenti.php", data=payload)
		soup = BeautifulSoup(r.content, "lxml")
		table = soup.select_one("table")
		with open(path+"/20"+f+".csv", "w") as f:
			wr = csv.writer(f)
			wr.writerows([[td.text.encode("utf-8").decode("utf-8").replace("\xa0", " ").strip() for td in row.find_all("td")] for row in table.select("tr")])

files = sorted([f for f in listdir(path) if isfile(join(path, f))])

# Main data-analysis cycle
for f in files:
	csvfile = csv.reader(open(path+'/'+f, newline=''))
	year, _ = f.split('.')

	tmpgend = collections.defaultdict(int)
	tmprole = collections.defaultdict(int)
	tmpuniv = collections.defaultdict(int)
	tmpregion = collections.defaultdict(int)
	tmparea = collections.defaultdict(int)
	tmpprof = collections.defaultdict(str)
	tmpprofuniv = collections.defaultdict(str)
	tmpprofgend = collections.defaultdict(str)
	firstrow = True

	for row in csvfile:
		if firstrow:
			firstrow = False
			continue
		tmprole[map_roles(row[0])] += 1
		tmpprof[row[1]] = map_roles(row[0])
		tmpprofgend[row[1]] = row[2]
		tmpprofuniv[row[1]] = row[3]
		tmpgend[row[2]] += 1
		tmpuniv[row[3]] += 1
		tmpregion[map_region(row[3])] += 1
		tmparea[map_area(map_region(row[3]))] += 1

	tupl = (int(year), tmpgend['M'], tmpgend['F'])
	gender.append(tupl)

	tupl = (int(year),)
	for k, v in tmprole.items():
		tupl = tupl + ((k, v),)
	role.append(tupl)

	tupl = (int(year),)
	for k, v in tmpuniv.items():
		tupl = tupl + ((k, v),)
	university.append(tupl)

	tupl = (int(year),)
	for k, v in tmpregion.items():
		tupl = tupl + ((k, v),)
	region.append(tupl)

	tupl = (int(year),)
	for k, v in tmparea.items():
		tupl = tupl + ((k, v),)
	area.append(tupl)

	tupl = (int(year),)
	for k, v in tmpprof.items():
		tupl = tupl + ((k, v),)
	prof.append(tupl)

	tupl = (int(year),)
	for k, v in tmpprofuniv.items():
		tupl = tupl + ((k, v),)
	profuniv.append(tupl)

	tupl = (int(year),)
	for k, v in tmpprofgend.items():
		tupl = tupl + ((k, v),)
	profgend.append(tupl)

# Plot regional distribution
for item in region:
	if (item[0] == minyear or item[0] == maxyear):
		plot_region_distribution(item[1:], item[0])

# Plot areas distribution
plot_areas_distribution(area)

# Plot gender distribution
plot_gender_distribution(gender)

# Plot role distribution
plot_role_distribution(role)

recruitments(prof, profgend, profuniv)

# mobility(profuniv)
